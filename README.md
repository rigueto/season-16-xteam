# README #

Essas configs foram desenvolvidas ao longo de 3 meses com ajuda de alguns players do Global.
O Beta já foi encerrado e só pretendo abrir um novo no S17 e encerrar após revisões, não tenho
pretensão de por server no ar. Aproveitem essa config e melhorem ela para seus devidos servidores.

### What is this repository for? ###

* Configuração completa Season 16 xteam.
* Faça suas devidas alterações para não ter server super idênticos.

### How do I get set up? ###

* Insira sua licença em todos lugares que possuem: CustomerLicenseId
* Faça seus ajustes de bags e até balanceamento.

### Elemental - Punish ###
Todos mobs tiveram os status recalculados com base no Global, aqui foi feito um trabalho gigante
pra tornar divertido e fazer sentido os status atuais dos char funcionarem.
Punish foi bem calculado para pegar corretamente.
Só lembrando que foi feito para um server sem reset.

### Skills ###
Todas skills foram ajustadas para funcionarem o mais próximo do Global, por isso
o ajuste em todos os mobs. (Isso é algo vivo pode ser que não esteja 100% igual mais).

### Labirinto/Maze Event 100% ###


### How about the settings/info? ###

### BASIC INFO ###
*Primeiros valores são do FREE e segundo é referente VIP.

Non Rebirth/Sem reset.
Experience Level 1 to 400	150x	200x
Master Level 401 to 800	30x	50x
Majestic Level 801 to 1300	30x~12x	50x~20x
Drop	20%	25%
Rate Jewels +1 to +9/Life	65%	75%
Bonus for 30min Online (GP)	10	25

### CHAOS MACHINE ###
Wing Lv. 1	80% + Luck	80% + Luck
Wing Lv. 2	60% + Luck	70% + Luck
Wing Lv. 3	45% + Luck	55% + Luck
Wing Lv. 4	30% + Luck	40% + Luck
Feather Of Condor	40%	45%
Feather Of Garuda	30%	40%
Piece of Horn	45%	50%
Broken Horn	40%	45%
Horn of Fenrir	30%	40%
Horn of Fenrir Upgrade	30%	40%
Earring Upgrade Level	60%	70%
Earring Up To +11~15	60%	70%
Ghost Horse Upgrade	50%	70%
Ice Dragon Rate 5	100%	100%
Ice Dragon Rate 4	80%	90%
Ice Dragon Rate 3	70%	80%
Ice Dragon Rate 2	60%	70%
Ice Dragon Rate 1	50%	60%
Up to +10 (comum, exc and 380)	60%	70%
Up to +11 (comum, exc and 380)	60%	70%
Up to +12 (comum, exc and 380)	60%	65%
Up to +13 (comum, exc and 380)	60%	65%
Up to +14 (comum, exc and 380)	55%	60%
Up to +15 (comum, exc and 380)	50%	60%
Item380	40%	50%
Socket Item Upgrade	60%	80%
Socket Item UnMount Seed Sphere	100%	100%

### PARTY BONUS EXPERIENCE ###
2 Players	EXP% + 80%	EXP% + 80%
3 Players	EXP% + 60%	EXP% + 75%
4 Players	EXP% + 50%	EXP% + 65%
5 Players	EXP% + 45%	EXP% + 60%

### RATES ERRTEL ###
Level	Lv +1	Lv +2	Lv +3	Lv +4	Lv +5	Lv +6	Lv +7	Lv +8	Lv +9	Lv +10
Rank 1	100%	100%	100%	90%	90%	80%	80%	70%	70%	70%
Rank 2	100%	100%	95%	90%	90%	80%	80%	70%	70%	65%
Rank 3	100%	95%	90%	90%	85%	85%	85%	65%	65%	60%

### ELEMENTS OF ERRTEL ###
Rank 1	Elemental Damage (16%)	Elemental Defense (16%)	Elemental A. S. Rate (16%)	Elemental D. S. Rate (16%)	Elemental Damage (16%)	Elemental Defense (16%)
Rank 2	Bastion Effect (33%)	Elemental HP Absorb Rate (33%)	Elemental SD Absorb Rate (33%)			
Rank 3	Punish Effect (20%)	Blindness Effect (20%)	Paralysis Effect (20%)	Hemorrhage Effect (20%)	Bondage Effect (20%)	

### RATES TO REMOVE ERRTEL ###
Level	Lv +0	Lv +1	Lv +2	Lv +3	Lv +4	Lv +5	Lv +6	Lv +7	Lv +8	Lv +9	Lv +10
Rank 1	100%	100%	100%	90%	90%	90%	70%	70%	70%	70%	70%
Rank 2	100%	100%	95%	90%	90%	80%	80%	70%	70%	65%	65%
Rank 3	100%	95%	90%	90%	85%	85%	85%	65%	65%	60%	60%
