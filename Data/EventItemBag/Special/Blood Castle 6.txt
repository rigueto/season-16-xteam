 3
//Index   DropRate
0         10000
1         10000
end

4
//Index   Section   SectionRate   MoneyAmount   OptionValue   DW   DK   FE   MG   DL   SU   RF   GL   RW   SL   GC
0         5         5000          2             32            1    1    1    1    1    1    1	 1    1    1    1  
0         17        5000          0             04            1    1    1    1    1    1    1	 1    1    1    1 
1         6         10000         2             32            1    0    0    0    0    0    0    0    0    0    0    
1         7         10000         2             32            0    1    0    0    0    0    0    0    0    0    0
1         8         10000         2             32            0    0    1    0    0    0    0    0    0    0    0
1         9         10000         2             32            0    0    0    1    0    0    0    0    0    0    0
1         10        10000         2             32            0    0    0    0    1    0    0    0    0    0    0
1         11        10000         2             32            0    0    0    0    0    1    0    0    0    0    0
1         12        10000         2             32            0    0    0    0    0    0    1    0    0    0    0
1         13        10000         2             32            0    0    0    0    0    0    0    1    0    0    0
1         14        10000         2             32            0    0    0    0    0    0    0    0    1    0    0
1         15        10000         2             32            0    0    0    0    0    0    0    0    0    1    0
1         16        10000         2             32            0    0    0    0    0    0    0    0    0    0    1
end

5 // Ruud
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration Comment 
14,489    0       0       *         *         *         *         *         *         *         0         //Ruud Box (1400)
end

17
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
14,013    0       4       *         *         *         *         *         *         *         0          //Jewel of Bless
14,014    0       3       *         *         *         *         *         *         *         0          //Jewel of Soul
end

6
//Itens Ancients top.
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
//DW E MG
3587      0       0       -1        12        12        -1        2         2         -1         0         //Enis-Anubis Legendary Helm
4099      0       0       -1        12        12        -1        2         2         -1         0         //Enis-Anubis Legendary Armor
6677      0       0       -1        12        12        -1        2         2         -1         0         //Anubis-Muren Ring of Fire
5123      0       0       -1        12        12        -1        2         2         -1         0         //Anubis Legendary Gloves
9,3       0       0       -1        12        12        -1        2         2         -1         0         //Enis Pants
11,3      0       0       -1        12        12        -1        2         2         -1         0         //Enis Boots
end

7
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
//DK E MG
3585      0       0       -1        12        12        -1        2         2         -1         0         //Hyon-Vicious Dragon Helm
0014      0       0       -1        12        12        -1        2         2         -1         0         //Hyon Lightning Sword
5121      0       0       -1        12        12        -1        2         2         -1         0         //Hyon Dragon Gloves
5633      0       0       -1        12        12        -1        2         2         -1         0         //Hyon Dragon Boots
8,1       0       0       -1        12        12        -1        2         2         -1         0         //Viciou Dragon Armor
9,1       0       0       -1        12        12        -1        2         2         -1         0         //Viciou Dragon Pants
end

8
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
//Elf
4110      0       0       -1        12        12        -1        2         2         -1         0         //Aruan-Gywen Guardian Armor
5646      0       0       -1        12        12        -1        2         2         -1         0         //Aruan-Gywen Guardian Boots
3598      0       0       -1        12        12        -1        2         2         -1         0         //Aruan-Guardian Helm
4622      0       0       -1        12        12        -1        2         2         -1         0         //Aruan-Guardian Pants
10,14     0       0       -1        12        12        -1        2         2         -1         0         //gywens glooves
4,5       0       0       -1        12        12        -1        2         2         -1         0         //silver bow
13,28     0       0       -1        12        12        -1        2         2         -1         0         //Pendant of Ability
end

9
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
//MG
4111      0       0       -1        12        12        -1        2         2         -1         0         //Gaion-Muren Storm Crow Armor
4623      0       0       -1        12        12        -1        2         2         -1         0         //Gaion-Muren Storm Crow Pants
6683      0       0       -1        12        12        -1        2         2         -1         0         //Alvis-Gaion Pendant of Water
5647      0       0       -1        12        12        -1        2         2         -1         0         //Gaion Storm Crow Boots
end

10
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
//DL
4634      0       0       -1        12        12        -1        2         2         -1         0         //Agnis-Broy Adamantine Pants
6665      0       0       -1        12        12        -1        2         2         -1         0         //Kantata-Agnis Ring of Poison
3610      0       0       -1        12        12        -1        2         2         -1         0         //Agnis Adamantine Mask
4122      0       0       -1        12        12        -1        2         2         -1         0         //Agnis Adamantine Armor
10,26     0       0       -1        12        12        -1        2         2         -1         0         //Broy Adamantine gloves
11,26     0       0       -1        12        12        -1        2         2         -1         0         //Broy Adamantine boots
end

11
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
//SUM
3624      0       0       -1        12        12        -1        2         2         -1         0         //Chrono-Semeden Red Wing Helm
5160      0       0       -1        12        12        -1        2         2         -1         0         //Chrono-Semeden Red Wing Gloves
6680      0       0       -1        12        12        -1        2         2         -1         0         //Apollo-Chrono Ring of Magic
4648      0       0       -1        12        12        -1        2         2         -1         0         //Chrono Red Wing Pants
8,40      0       0       -1        12        12        -1        2         2         -1         0         //Armor semeden
11,40     0       0       -1        12        12        -1        2         2         -1         0         //Boot Semeden
end

12
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
//RF
0032      0       0       -1        12        12        -1        2         2         -1         0         //Vega-Chamer Sacred Glove
4155      0       0       -1        12        12        -1        2         2         -1         0         //Vega-Chamer Sacred Fire Armor
4667      0       0       -1        12        12        -1        2         2         -1         0         //Vega-Chamer Sacred Fire Pants
5691      0       0       -1        12        12        -1        2         2         -1         0         //Chamer Sacred Fire Boots
3643      0       0       -1        12        12        -1        2         2         -1         0         //Sacred Fire Helm"
end

13
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
//GL
3679      0       0       -1        12        12        -1        2         2         -1         0         //Carthy Sate Set
4191      0       0       -1        12        12        -1        2         2         -1         0         //Camill Sate Set
4703      0       0       -1        12        12        -1        2         2         -1         0         //Carthy Sate Set
5215      0       0       -1        12        12        -1        2         2         -1         0         //Camill Sate Set
5727      0       0       -1        12        12        -1        2         2         -1         0         //Camill Sate Set
3678      0       0       -1        12        12        -1        2         2         -1         0         //Akhir Hirat Set
4190      0       0       -1        12        12        -1        2         2         -1         0         //Akhir Hirat Set
4702      0       0       -1        12        12        -1        2         2         -1         0         //Anas Hirat Set
5214      0       0       -1        12        12        -1        2         2         -1         0         //Anas Hirat Set
5726      0       0       -1        12        12        -1        2         2         -1         0         //Anas Hirat Set
6664      0       0       -1        12        12        -1        2         2         -1         0         //Ring of Ice   
6665      0       0       -1        12        12        -1        2         2         -1         0         //Ring of Poison
end

14
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
//RM
3114      0       0       -1        12        12        -1        2         2         -1         0         //Emile's shield el hazard
4727      0       0       -1        12        12        -1        2         2         -1         0         //Arcajins Kanaz
5239      0       0       -1        12        12        -1        2         2         -1         0         //Arcajins Kanaz 
5751      0       0       -1        12        12        -1        2         2         -1         0         //Arcajins Kanaz 
7,119     0       0       -1        12        12        -1        2         2         -1         0         //Emile's bandana
8,119     0       0       -1        12        12        -1        2         2         -1         0         //Arcajins's armor
end

15
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
//SLAYER
5809      0       0       -1        12        12        -1        2         2         -1         0         //Verpers Nightwing
3761      0       0       -1        12        12        -1        2         2         -1         0         //Molossos Nightwing         
4273      0       0       -1        12        12        -1        2         2         -1         0         //slayer Nightwing        
5297      0       0       -1        12        12        -1        2         2         -1         0         //slayer Nightwing  
4785      0       0       -1        12        12        -1        2         2         -1         0         //slayer Nightwing
end

16
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
//Gun
7,209     0       0       -1        12        12        -1        2         2         -1         0         //Bruning Gun Frere
8,209     0       0       -1        12        12        -1        2         2         -1         0         //Falcons Gun Frere         
9,209     0       0       -1        12        12        -1        2         2         -1         0         //Gun Frere        
11,209    0       0       -1        12        12        -1        2         2         -1         0         //Gun Frere  
end