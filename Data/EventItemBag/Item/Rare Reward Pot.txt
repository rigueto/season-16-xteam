3
//Index   DropRate
0         10000
1         10000
end

//O correto em MoneyAmount é código 3 do Illusion, porem nao enviar itens com prazo pro inv do K.
4
//Index   Section   SectionRate   MoneyAmount   OptionValue   DW   DK   FE   MG   DL   SU   RF   GL   RW   SL   GC
0         5         10000         100           0             1    1    1    1    1    1    1    1    1    1    1    
1         6         10000         100           0             1    1    1    1    1    1    1    1    1    1    1   
end

5
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
6410      0       0       -1        -1        -1        -1        -1        -1         -1        259200    //Wings of Conqueror"     
6411      0       0       -1        -1        -1        -1        -1        -1         -1        259200    //Wings of Angel and Devil
6412      0       0       -1        -1        -1        -1        -1        -1         -1        259200    //Wings of Conqueror"     
6827      0       0       -1        -1        -1        -1        -1        -1         -1        259200    //Necklace of Agony"    
6828      0       0       -1        -1        -1        -1        -1        -1         -1        259200    //Solid Symbol"         
6829      0       0       -1        -1        -1        -1        -1        -1         -1        259200    //Ring of Ultimatum"    
6830      0       0       -1        -1        -1        -1        -1        -1         -1        259200    //Ring of Block"        
6831      0       0       -1        -1        -1        -1        -1        -1         -1        259200    //Protection Ring"      
6832      0       0       -1        -1        -1        -1        -1        -1         -1        259200    //Protection Ring"
end

6
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
//Rank 1 +0
12,221    0        0        *         *         *         *         *         *         #001,255,255,255,255        0          //Errtel of Anger
12,231    0        0        *         *         *         *         *         *         #001,255,255,255,255        0          //Errtel of Blessing
12,241    0        0        *         *         *         *         *         *         #001,255,255,255,255        0          //Errtel of Integrity
12,251    0        0        *         *         *         *         *         *         #001,255,255,255,255        0          //Errtel of Divinity
//Rank 1 +1                                                                                  
12,221    1        0        *         *         *         *         *         *         #017,255,255,255,255        0          //Errtel of Anger
12,231    1        0        *         *         *         *         *         *         #017,255,255,255,255        0          //Errtel of Blessing
12,241    1        0        *         *         *         *         *         *         #017,255,255,255,255        0          //Errtel of Integrity
12,251    1        0        *         *         *         *         *         *         #017,255,255,255,255        0          //Errtel of Divinity
//Rank 1 +2                                                                                  
12,221    2        0        *         *         *         *         *         *         #033,255,255,255,255        0          //Errtel of Anger
12,231    2        0        *         *         *         *         *         *         #033,255,255,255,255        0          //Errtel of Blessing
12,241    2        0        *         *         *         *         *         *         #033,255,255,255,255        0          //Errtel of Integrity
12,251    2        0        *         *         *         *         *         *         #033,255,255,255,255        0          //Errtel of Divinity
//Rank 1 +3                                                                                  
12,221    3        0        *         *         *         *         *         *         #049,255,255,255,255        0          //Errtel of Anger
12,231    3        0        *         *         *         *         *         *         #049,255,255,255,255        0          //Errtel of Blessing
12,241    3        0        *         *         *         *         *         *         #049,255,255,255,255        0          //Errtel of Integrity
12,251    3        0        *         *         *         *         *         *         #049,255,255,255,255        0          //Errtel of Divinity
//Rank 1 +4                                                                                  
12,221    4        0        *         *         *         *         *         *         #065,255,255,255,255        0          //Errtel of Anger
12,231    4        0        *         *         *         *         *         *         #065,255,255,255,255        0          //Errtel of Blessing
12,241    4        0        *         *         *         *         *         *         #065,255,255,255,255        0          //Errtel of Integrity
12,251    4        0        *         *         *         *         *         *         #065,255,255,255,255        0          //Errtel of Divinity
//Rank 1 +5                                                                                  
12,221    5        0        *         *         *         *         *         *         #081,255,255,255,255        0          //Errtel of Anger
12,231    5        0        *         *         *         *         *         *         #081,255,255,255,255        0          //Errtel of Blessing
12,241    5        0        *         *         *         *         *         *         #081,255,255,255,255        0          //Errtel of Integrity
12,251    5        0        *         *         *         *         *         *         #081,255,255,255,255        0          //Errtel of Divinity
//Rank 1 +6                                                                                  
12,221    6        0        *         *         *         *         *         *         #097,255,255,255,255        0          //Errtel of Anger
12,231    6        0        *         *         *         *         *         *         #097,255,255,255,255        0          //Errtel of Blessing
12,241    6        0        *         *         *         *         *         *         #097,255,255,255,255        0          //Errtel of Integrity
12,251    6        0        *         *         *         *         *         *         #097,255,255,255,255        0          //Errtel of Divinity
end