3
//Index   DropRate
0         10000
end

4
//Index   Section   SectionRate   MoneyAmount   OptionValue   DW   DK   FE   MG   DL   SU   RF   GL   RW   SL   GC
0         5         3000          0             0             1    1    1    1    1    1    1    1    1    1    1
0         6         7000          0             0             1    1    1    1    1    1    1    1    1    1    1
end

5
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
8403      0       *       *         *         *         *         *         *         *         0          //Evolution Stone
end

6
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
8194     1       0       *         *         *         *         *         *         *         0          //Geppetto
8196     1       0       *         *         *         *         *         *         *         0          //BlueFairy
8200     1       0       *         *         *         *         *         *         *         0          //BabyBear
8208     1       0       *         *         *         *         *         *         *         0          //TreasureHunter
8210     1       0       *         *         *         *         *         *         *         0          //TreeElf
8212     1       0       *         *         *         *         *         *         *         0          //GrassFairy
8216     1       0       *         *         *         *         *         *         *         0          //SambaGirl
8218     1       0       *         *         *         *         *         *         *         0          //Rio
8220     1       0       *         *         *         *         *         *         *         0          //Bateria
8224     1       0       *         *         *         *         *         *         *         0          //Tiger
8226     1       0       *         *         *         *         *         *         *         0          //Libra
8228     1       0       *         *         *         *         *         *         *         0          //Scorpio
8230     1       0       *         *         *         *         *         *         *         0          //Rabbit
8233     1       0       *         *         *         *         *         *         *         0          //Chicken
8241     1       0       *         *         *         *         *         *         *         0          //KnightInstructor
8247     1       0       *         *         *         *         *         *         *         0          //Adventurecat
8249     1       0       *         *         *         *         *         *         *         0          //Dike
8253     1       0       *         *         *         *         *         *         *         0          //Scarecrow
8259     1       0       *         *         *         *         *         *         *         0          //SnowFairy
8275     1       0       *         *         *         *         *         *         *         0          //Pilot
8277     1       0       *         *         *         *         *         *         *         0          //SuperPanda
8279     1       0       *         *         *         *         *         *         *         0          //BellyDancer
8289     1       0       *         *         *         *         *         *         *         0          //BeachGirl
8297     1       0       *         *         *         *         *         *         *         0          //FlyingBread
8306     1       0       *         *         *         *         *         *         *         0          //Serufan
8308     1       0       *         *         *         *         *         *         *         0          //Medusa
8310     1       0       *         *         *         *         *         *         *         0          //Balrog
8312     1       0       *         *         *         *         *         *         *         0          //Hellmine
8316     1       0       *         *         *         *         *         *         *         0          //Hydra
8328     1       0       *         *         *         *         *         *         *         0          //Desmodus
8403     1       0       *         *         *         *         *         *         *         0          //EvolutionStone
8421     1       0       *         *         *         *         *         *         *         0          //Wooparoo
8427     1       0       *         *         *         *         *         *         *         0          //Skull
8431     1       0       *         *         *         *         *         *         *         0          //MuunEnergyConverter
8432     1       0       *         *         *         *         *         *         *         0          //WizardryStone
8435     1       0       *         *         *         *         *         *         *         0          //Lycan
8444     1       0       *         *         *         *         *         *         *         0          //Seiren
8446     1       0       *         *         *         *         *         *         *         0          //Behemoth
8450     1       0       *         *         *         *         *         *         *         0          //Mino
8452     1       0       *         *         *         *         *         *         *         0          //Hound
8458     1       0       *         *         *         *         *         *         *         0          //Cyclop
8464     1       0       *         *         *         *         *         *         *         0          //Hawk
8470     1       0       *         *         *         *         *         *         *         0          //Mantis
8474     1       0       *         *         *         *         *         *         *         0          //Chafer
8486     1       0       *         *         *         *         *         *         *         0          //SportyGirl
8494     1       0       *         *         *         *         *         *         *         0          //Frozen
8496     1       0       *         *         *         *         *         *         *         0          //Lure
8500     1       0       *         *         *         *         *         *         *         0          //LuckyRabbit
8514     1       0       *         *         *         *         *         *         *         0          //Kenturion
8516     1       0       *         *         *         *         *         *         *         0          //Rocka
8522     1       0       *         *         *         *         *         *         *         0          //Griffs
8524     1       0       *         *         *         *         *         *         *         0          //PolarBear
8526     1       0       *         *         *         *         *         *         *         0          //Present
8528     1       0       *         *         *         *         *         *         *         0          //RockingHorse
8532     1       0       *         *         *         *         *         *         *         0          //Lester
8534     1       0       *         *         *         *         *         *         *         0          //Merino
8536     1       0       *         *         *         *         *         *         *         0          //Splinter
8538     1       0       *         *         *         *         *         *         *         0          //Mutation
8540     1       0       *         *         *         *         *         *         *         0          //Fighter
8601     1       0       *         *         *         *         *         *         *         0          //Heywood
8603     1       0       *         *         *         *         *         *         *         0          //Branch
8605     1       0       *         *         *         *         *         *         *         0          //Hook
8609     1       0       *         *         *         *         *         *         *         0          //Mandora
8611     1       0       *         *         *         *         *         *         *         0          //Treat
8619     1       0       *         *         *         *         *         *         *         0          //Gigantes
8623     1       0       *         *         *         *         *         *         *         0          //Walker
8625     1       0       *         *         *         *         *         *         *         0          //SurfingGirl
8631     1       0       *         *         *         *         *         *         *         0          //Commander
8633     1       0       *         *         *         *         *         *         *         0          //LanceSoldier
8635     1       0       *         *         *         *         *         *         *         0          //BowSoldier
8637     1       0       *         *         *         *         *         *         *         0          //Hammer
8643     1       0       *         *         *         *         *         *         *         0          //PumpkinGirl
8645     1       0       *         *         *         *         *         *         *         0          //Ghost
8647     1       0       *         *         *         *         *         *         *         0          //MummyBoy
8649     1       0       *         *         *         *         *         *         *         0          //Chrysanthemum
8651     1       0       *         *         *         *         *         *         *         0          //Maple
8653     1       0       *         *         *         *         *         *         *         0          //Ginkgo
8672     1       0       *         *         *         *         *         *         *         0          //ToySoldier
8676     1       0       *         *         *         *         *         *         *         0          //Snowman
8678     1       0       *         *         *         *         *         *         *         0          //BuskerMonkey
8680     1       0       *         *         *         *         *         *         *         0          //DJMonkey
8682     1       0       *         *         *         *         *         *         *         0          //SpaceMonkey
8684     1       0       *         *         *         *         *         *         *         0          //LittleCupid
8688     1       0       *         *         *         *         *         *         *         0          //HeartGirl
8690     1       0       *         *         *         *         *         *         *         0          //CandyGirl
8692     1       0       *         *         *         *         *         *         *         0          //LollipopMama
8694     1       0       *         *         *         *         *         *         *         0          //SweetMuscleMan
//8324     1       0       *         *         *         *         *         *         *         0          //KAikuru


end