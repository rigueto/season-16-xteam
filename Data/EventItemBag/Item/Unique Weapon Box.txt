3
//Index   DropRate
0         10000
end

4
//Index   Section   SectionRate   MoneyAmount   OptionValue   DW   DK   FE   MG   DL   SU   RF   GL  RW  SL  GC
0         5         10000          100           0            1    1    1    1    1    1    1    1   1   1   1
end

5
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration     Comment
2581      0       1       *         12        13        20        21        *         *         0               //Book of Shamu
2582      0       1       *         12        13        20        21        *         *         0               //Book of Neil"
2583      0       1       *         12        13        20        21        *         *         0               //Book of Lagle
2080      0       1       *         12        13        20        21        *         *         0               //Combat Quiver"
3114      0       1       *         12        13        20        21        *         *         0               //El Hazard Shield
3102      0       1       *         12        13        20        21        *         *         0               //Pluma Shield
3101      0       1       *         12        13        20        21        *         *         0               //Lapid Shield"              
3087      0       1       *         12        13        20        21        *         *         0               //Grand Soul Shield"          
3088      0       1       *         12        13        20        21        *         *         0               //Elemental Shield"           
3093      0       1       *         12        13        20        21        *         *         0               //Cross Shield
3103      0       1       *         12        13        20        21        *         *         0               //Alacran Shield" 
3072      0       1       *         12        13        20        21        *         *         0               //Small Shield"          
3073      0       1       *         12        13        20        21        *         *         0               //Horn Shield"           
3074      0       1       *         12        13        20        21        *         *         0               //Kite Shield"           
3075      0       1       *         12        13        20        21        *         *         0               //Elven Shield"          
3076      0       1       *         12        13        20        21        *         *         0               //Buckler"               
3077      0       1       *         12        13        20        21        *         *         0               //Dragon Slayer Shield"  
3078      0       1       *         12        13        20        21        *         *         0               //Skull Shield"          
3079      0       1       *         12        13        20        21        *         *         0               //Spiked Shield"         
3080      0       1       *         12        13        20        21        *         *         0               //Tower Shield"          
3081      0       1       *         12        13        20        21        *         *         0               //Plate Shield"          
3082      0       1       *         12        13        20        21        *         *         0               //Large Round Shield"    
3083      0       1       *         12        13        20        21        *         *         0               //Serpent Shield"        
3084      0       1       *         12        13        20        21        *         *         0               //Bronze Shield"         
3085      0       1       *         12        13        20        21        *         *         0               //Dragon Shield"         
3086      0       1       *         12        13        20        21        *         *         0               //Legendary Shield"      
end