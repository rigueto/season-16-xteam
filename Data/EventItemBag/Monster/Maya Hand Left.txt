 3
//Index   DropRate
0         10000
end

4
//Index   Section   SectionRate   MoneyAmount   OptionValue   DW   DK   FE   MG   DL   SU   RF   GL   RW   SL   GC
0         5         10000         100           32            1    1    1    1    1    1    1    1    1    1    1     
end

5
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration     Comment
// Armas Box of Kundun +5
0020      0       1       *         12        13        20        602       *         *         0               //Knight Blade Sword
0024	  0       1       *         12        13        20        602       *         *         0               //Daybreak
0021	  0       1       *         12        13        20        602       *         *         0               //Dark Reign Blade Sword
0025	  0       1       *         12        13        20        602       *         *         0               //Sword Dancer
1036      0       1       *         12        13        20        602       *         *         0               //Great Lord Scepter
1039      0       1       *         12        13        20        602       *         *         0               //Shining Scepter
1546      0       1       *         12        13        20        602       *         *         0               //Dragon Spear
2067      0       1       *         12        13        20        602       *         *         0               //Great Reign Crossbow
2068      0       1       *         12        13        20        602       *         *         0               //Arrow Viper CrossBow
2070      0       1       *         12        13        20        602       *         *         0               //Albatross Bow
2571      0       1       *         12        13        20        602       *         *         0               //Staff of Kundun
2573      0       1       *         12        13        20        602       *         *         0               //Platina Staff
2578      0       1       *         12        13        20        602       *         *         0               //Demonic Stick
2579      0       1       *         12        13        20        602       *         *         0               //Storm Blitz Stick
1551      0       1       *         12        13        20        602       *         *         0               //Pluma lancer
1552      0       1       *         12        13        20        602       *         *         0               //Vis lancer
0033	  0       1       *         12        13        20        602       *         *         0         		//Holy Storm Claw
0034      0       1       *         12        13        20        602       *         *         0               //Piercing Blade Glove
1053      0       1       *         12        13        20        602       *         *         0               //El hazard mace
1052      0       1       *         12        13        20        602       *         *         0               //Elemental Rune Mace
0067      0       1       *         12        13        20        602       *         *         0               //Short Sword Dacia
0068      0       1       *         12        13        20        602       *         *         0               //Short Sword Cookery
4,44      0       1       *         12        13        20        602       *         *         0               //Entropy Magic Gun
4,43      0       1       *         12        13        20        602       *         *         0               //Frere Magic Gun

//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Great Dragon (dk)
3605      0       1       *         *         13        20        602       *         *         0          	//Helm Great Dragon
4117      0       1       *         *         13        20        602       *         *         0          	//Armo Great Dragon
4629      0       1       *         *         13        20        602       *         *         0          	//Pants Great Dragon
5141      0       1       *         *         13        20        602       *         *         0          	//Gloves Great Dragon
5653      0       1       *         *         13        20        602       *         *         0          	//Boots Great Dragon
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration  Comment 
// Set Ashcrow (dk)
3618      0       1       *         *         13        20        602       *         *         0          	//Helm Ashcrow
4130      0       1       *         *         13        20        602       *         *         0          	//Armo Ashcrow
4642      0       1       *         *         13        20        602       *         *         0          	//Pants Ashcrow
5154      0       1       *         *         13        20        602       *         *         0          	//Gloves Ashcrow
5666      0       1       *         *         13        20        602       *         *         0          	//Boots Ashcrow
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Dark Soul (dw)
3606      0       1       *         *         13        20        602       *         *         0          	//Helm Dark Soul
4118      0       1       *         *         13        20        602       *         *         0          	//Armo Dark Soul
4630      0       1       *         *         13        20        602       *         *         0          	//Pants Dark Soul
5142      0       1       *         *         13        20        602       *         *         0          	//Gloves Dark Soul
5654      0       1       *         *         13        20        602       *         *         0          	//Boots Dark Soul
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Eclipse (dw)
3619      0       1       *         *         13        20        602       *         *         0          	//Helm Eclipse
4131      0       1       *         *         13        20        602       *         *         0          	//Armo Eclipse
4643      0       1       *         *         13        20        602       *         *         0          	//Pants Eclipse
5155      0       1       *         *         13        20        602       *         *         0          	//Gloves Eclipse
5667      0       1       *         *         13        20        602       *         *         0          	//Boots Eclipse
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Red Spirit (elf)
3608      0       1       *         *         13        20        602       *         *         0          	//Helm Red Spirit
4120      0       1       *         *         13        20        602       *         *         0          	//Armo Red Spirit
4632      0       1       *         *         13        20        602       *         *         0          	//Pants Red Spirit
5144      0       1       *         *         13        20        602       *         *         0          	//Gloves Red Spirit
5656      0       1       *         *         13        20        602       *         *         0          	//Boots Red Spirit
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Iris (elf)
3620      0       1       *         *         13        20        602       *         *         0          	//Helm Iris
4132      0       1       *         *         13        20        602       *         *         0          	//Armo Iris
4644      0       1       *         *         13        20        602       *         *         0          	//Pants Iris
5156      0       1       *         *         13        20        602       *         *         0          	//Gloves Iris
5668      0       1       *         *         13        20        602       *         *         0          	//Boots Iris
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Hurricane (mg)
4119      0       1       *         *         13        20        602       *         *         0          	//Armo Hurricane
4631      0       1       *         *         13        20        602       *         *         0          	//Pants Hurricane
5143      0       1       *         *         13        20        602       *         *         0          	//Gloves Hurricane
5655      0       1       *         *         13        20        602       *         *         0          	//Boots Hurricane
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Valiant (mg)
4133      0       1       *         *         13        20        602       *         *         0          	//Armo Valiant
4645      0       1       *         *         13        20        602       *         *         0          	//Pants Valiant
5157      0       1       *         *         13        20        602       *         *         0          	//Gloves Valiant
5669      0       1       *         *         13        20        602       *         *         0          	//Boots Valiant
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Dark Master (dl)
3612      0       1       *         *         13        20        602       *         *         0          	//Helm Dark Master
4124      0       1       *         *         13        20        602       *         *         0          	//Armo Dark Master
4636      0       1       *         *         13        20        602       *         *         0          	//Pants Dark Master
5148      0       1       *         *         13        20        602       *         *         0          	//Gloves Dark Master
5660      0       1       *         *         13        20        602       *         *         0          	//Boots Dark Master
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Glorious (dl)
3622      0       1       *         *         13        20        602       *         *         0          	//Helm Glorious
4134      0       1       *         *         13        20        602       *         *         0          	//Armo Glorious
4646      0       1       *         *         13        20        602       *         *         0          	//Pants Glorious
5158      0       1       *         *         13        20        602       *         *         0          	//Gloves Glorious
5670      0       1       *         *         13        20        602       *         *         0          	//Boots Glorious
//Set Hirat GL
3678      0       1       *         *         13        20        602       *         *         0          	//
4190      0       1       *         *         13        20        602       *         *         0          	// 
4702      0       1       *         *         13        20        602       *         *         0          	// 
5214      0       1       *         *         13        20        602       *         *         0          	// 
5726      0       1       *         *         13        20        602       *         *         0          	// 
//Set Sate GL
3679      0       1       *         *         13        20        602       *         *         0          	//
4191      0       1       *         *         13        20        602       *         *         0          	//  
4703      0       1       *         *         13        20        602       *         *         0          	//  
5215      0       1       *         *         13        20        602       *         *         0          	//  
5727      0       1       *         *         13        20        602       *         *         0          	//  
//Set kanaz RM
3703      0       1       *         *         13        20        602       *         *         0         	//
4215      0       1       *         *         13        20        602       *         *         0         	//
4727      0       1       *         *         13        20        602       *         *         0         	//
5239      0       1       *         *         13        20        602       *         *         0         	//
5751      0       1       *         *         13        20        602       *         *         0         	//
//Set Legendary RM
3702      0       1       *         *         13        20        602       *         *         0          	//
4214      0       1       *         *         13        20        602       *         *         0          	//
4726      0       1       *         *         13        20        602       *         *         0          	//
5238      0       1       *         *         13        20        602       *         *         0          	//
5633      0       1       *         *         13        20        602       *         *         0          	//
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Demonic (Sum)
3626      0       1       *         *         13        20        602       *         *         0          	//Helm Demonic
4138      0       1       *         *         13        20        602       *         *         0          	//Armo Demonic
4650      0       1       *         *         13        20        602       *         *         0          	//Pants Demonic
5162      0       1       *         *         13        20        602       *         *         0          	//Gloves Demonic
5674      0       1       *         *         13        20        602       *         *         0          	//Boots Demonic
//Set Storm Blitz (Sum)
3627      0       1       *         *         13        20        602       *         *         0          	//
4139      0       1       *         *         13        20        602       *         *         0          	//
4651      0       1       *         *         13        20        602       *         *         0          	//
5163      0       1       *         *         13        20        602       *         *         0          	//
5675      0       1       *         *         13        20        602       *         *         0          	//
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
// Set Piercing Groove (RF)
3645      0       1       *         *         13        20        602       *         *         0          	//Helm Piercing Groove
4157      0       1       *         *         13        20        602       *         *         0          	//Armo Piercing Groove
4669      0       1       *         *         13        20        602       *         *         0          	//Pants Piercing Groove
5693      0       1       *         *         13        20        602       *         *         0          	//Boots Piercing Groove
//Set Storm jahad (RF)
3644      0       1       *         *         13        20        602       *         *         0          	//
4156      0       1       *         *         13        20        602       *         *         0          	//
4668      0       1       *         *         13        20        602       *         *         0          	//
5692      0       1       *         *         13        20        602       *         *         0          	//
//Set Demoniac A SL
3760      0       1       *         *         13        20        602       *         *         0          	//
4272      0       1       *         *         13        20        602       *         *         0          	//
4784      0       1       *         *         13        20        602       *         *         0          	//
5296      0       1       *         *         13        20        602       *         *         0          	//
5808      0       1       *         *         13        20        602       *         *         0          	//
//Set Nightwing SL
3761      0       1       *         *         13        20        602       *         *         0          	//
4273      0       1       *         *         13        20        602       *         *         0          	//
4785      0       1       *         *         13        20        602       *         *         0          	//
5297      0       1       *         *         13        20        602       *         *         0          	//
5809      0       1       *         *         13        20        602       *         *         0          	//

//Gun Frere
7,209     0       1       *         *         13        20        602       *         *         0          	//
8,209     0       1       *         *         13        20        602       *         *         0          	//
9,209     0       1       *         *         13        20        602       *         *         0          	//
10,20     0       1       *         *         13        20        602       *         *         0          	//
11,20     0       1       *         *         13        20        602       *         *         0          	//

14,013    0       0       *         *         *         *         *         *         *         0          //Jewel of Bless
14,014    0       0       *         *         *         *         *         *         *         0          //Jewel of Soul
12,015    0       0       *         *         *         *         *         *         *         0          //Jewel of Chaos

end