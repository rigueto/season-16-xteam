--BridgeFunctionAttach("OnItemPick","itemVip");
--
--function itemVip(aIndex,index,ItemTable)
--
--	LogPrint("Peguei item");
--
--    if (InventoryGetItemCount(aIndex,6669,-1) == 1) then
--        LogPrint('chuupa');
--    end
--    
--    for x = 0, 300, 1 do
--
--        if (InventoryGetItemIndex(aIndex, x) == 6669) then
--            LogPrint('posicao do item no inventory');
--			LogPrint(x);
--            break;
--        end
--    end
--\par
--\b OnItemPick(aIndex,index,ItemTable)\par
--\par
--\b0 aIndex = User index.\par
--index = Item slot.\par
--ItemTable = Item information table.\par
--\par
--Called on item pick, must return (1) if the item can be picked, (0) if not.\b\par
--\b0\par
--end  


PKOFF = {}
PKOFF.OFFAREA = { -- Areas onde o PVP não gera PK 
    { 
        Map = 0,      -- Mapa
        MapXMin = 111,  -- Posição X Minima no mapa
        MapYMin = 102,   -- Posição Y Minima no mapa
        MapXMax = 155,  -- Posição X Máxima no mapa
      	MapYMax = 150   -- Posição Y Máxima no mapa
    }
}

--Coord vila toda de Lorencia, devido aos moves dos eventos.
--111 102
--111 150
--155 102
--155 150

--Coordenadas apenas do ringue
--139
--123
--148
--132

BridgeFunctionAttach("OnCheckUserKiller","PKOFF_OnCheckUserKiller")
--PKOFF_OnCheckUserKiller = function(aIndex,bIndex)
function PKOFF_OnCheckUserKiller(aIndex)  
	--LogPrint("teste2")
	for k,v in pairs(PKOFF.OFFAREA) do
		
		local MapX = GetObjectMapX(aIndex)
		local MapY = GetObjectMapY(aIndex)
		
		if GetObjectMap(aIndex) == v.Map and MapX >= v.MapXMin and MapX <= v.MapXMax and MapY >= v.MapYMin and MapY <= v.MapYMax then
			
			return 0
			
		end
		
	end
	
	return 1
	
end


BridgeFunctionAttach("OnUserDie","OnUserDie_Ringue")

function OnUserDie_Ringue(aIndex, bIndex)  
	for k,v in pairs(PKOFF.OFFAREA) do
		
		local MapX = GetObjectMapX(aIndex)
		local MapY = GetObjectMapY(aIndex)
		
		if GetObjectMap(aIndex) == v.Map and MapX >= v.MapXMin and MapX <= v.MapXMax and MapY >= v.MapYMin and MapY <= v.MapYMax then
			
			NoticeSendToAll(0, "**Ringue de Lorencia** O Player "..GetObjectName(bIndex).." matou o "..GetObjectName(aIndex));
			
		end
	end  
end
 