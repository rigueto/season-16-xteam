StarterPack_SystemSwitch = 1

BridgeFunctionAttach("OnReadScript","StarterPack_OnReadScript")

BridgeFunctionAttach("OnCharacterEntry","StarterPack_OnCharacterEntry")

BridgeFunctionAttach("OnSQLAsyncResult","StarterPack_OnSQLAsyncResult")

function StarterPack_OnReadScript()

	local SQL_ODBC = "MuOnline"

	local SQL_USER = "sa"

	local SQL_PASS = "P@ssw0rdD06m06"

	SQLAsyncConnect(SQL_ODBC,SQL_USER,SQL_PASS)

	--Para outras conexões SQL
	SQLConnect('MuOnline','sa','P@ssw0rdD06m06');
end


function StarterPack_OnCharacterEntry(aIndex)
	--or GetObjectLevel(aIndex) == 400 or GetObjectMasterLevel(aIndex) >= 300
	if (StarterPack_SystemSwitch ~= 0 and GetObjectLevel(aIndex) == 1) then
		SQLAsyncQuery(string.format("SELECT Status FROM StarterPackData WHERE CharName='"..GetObjectName(aIndex).."' and AccountID='"..GetObjectAccount(aIndex).."'"),"StarterPack_QUERY1",string.format("%d",aIndex))
	
		SQLAsyncQuery(string.format("SELECT Status FROM StarterPackEvent WHERE AccountID='%s'",GetObjectAccount(aIndex)),"StarterPack_Event",string.format("%d",aIndex))
	end
end


function StarterPack_OnSQLAsyncResult(label,param,result)

	local StarterPackStatus = SQLAsyncGetNumber("Status");

	if (label == "StarterPack_QUERY1") then

		local aIndex = CommandGetArgNumber(param,0)

		if (GetObjectLevel(aIndex) == 1) then
			if(result == 0) then
				SQLAsyncQuery(string.format("INSERT INTO StarterPackData (AccountID,CharName,Status) VALUES ('"..GetObjectAccount(aIndex).."','"..GetObjectName(aIndex).."',1)"))
				StarterPack_GiveStarterPack(aIndex)
			end	
		--elseif (StarterPackStatus == 1 and GetObjectLevel(aIndex) == 400 and GetObjectMasterLevel(aIndex) == 0) then
		--	
		--	SQLAsyncQuery(string.format("UPDATE StarterPackData SET Status=2 WHERE AccountID='%s'",GetObjectAccount(aIndex)))
		--	StarterPack_GiveStarterPack(aIndex)
		--elseif (StarterPackStatus == 2 and GetObjectLevel(aIndex) == 400 and GetObjectMasterLevel(aIndex) >= 300) then
		--	SQLAsyncQuery(string.format("UPDATE StarterPackData SET Status=3 WHERE AccountID='%s'",GetObjectAccount(aIndex)))
		--	StarterPack_GiveStarterPack(aIndex)
		end

		return 1

	end
	
	if (label == "StarterPack_Event") then

		local aIndex = CommandGetArgNumber(param,0)

		if (GetObjectLevel(aIndex) == 1) then
			if(result == 0) then
				SQLAsyncQuery(string.format("INSERT INTO StarterPackEvent (AccountID,CharName,Status) VALUES ('"..GetObjectAccount(aIndex).."','"..GetObjectName(aIndex).."',1)"))
				StarterPack_GiveStarterPackEvent(aIndex)
			end	
		end
		return 1
	end

	return 0

end


function StarterPack_GiveStarterPack(aIndex)

    local UserClass = GetObjectClass(aIndex)
	LoadFileTxt(aIndex);

	for n=1,#StarterPack_ItemList,1 do
		if StarterPack_ItemList[n].Class == -1 or StarterPack_ItemList[n].Class == UserClass then

			if StarterPack_ItemList[n].Group == 0 then

				ItemGiveEx(aIndex,StarterPack_ItemList[n].Index,StarterPack_ItemList[n].Level,StarterPack_ItemList[n].Count,StarterPack_ItemList[n].Option1,StarterPack_ItemList[n].Option2,StarterPack_ItemList[n].Option3,StarterPack_ItemList[n].NewOption)

			elseif StarterPack_ItemList[n].Group == 1 then

				EffectAdd(aIndex,1,StarterPack_ItemList[n].Index,StarterPack_ItemList[n].Count,0,0,0,0)

			end

		end

	end
	NoticeSend(aIndex,1,"Are you received your kit name: "..NamePack);
end

function LoadFileTxt(aIndex) 
	StarterPack_ItemList = {}

	if(GetObjectLevel(aIndex) == 1 and GetObjectMasterLevel(aIndex) == 0) then
		fileStartPack = Reader:Load("..\\Data\\Script\\Data\\StarterPack.txt")
		NamePack = 'Beginner.';  
		  
	--elseif(GetObjectLevel(aIndex) == 400 and GetObjectMasterLevel(aIndex) == 0) then
	--	fileStartPack = Reader:Load("..\\Data\\Script\\Data\\StarterPack400.txt");
	--	NamePack = 'Lvl 400.';
	--	
	--elseif(GetObjectLevel(aIndex) == 400 and GetObjectMasterLevel(aIndex) >= 300) then
	--	fileStartPack = Reader:Load("..\\Data\\Script\\Data\\StarterPack700.txt");
    --    NamePack = 'Lvl 700';
	end

	if fileStartPack ~= nil then

		while fileStartPack:GetLine("end") == true do

			local StarterPack_ItemListRow = {}

			StarterPack_ItemListRow["Class"] = fileStartPack:GetAsNumber()

			StarterPack_ItemListRow["Group"] = fileStartPack:GetAsNumber()

			StarterPack_ItemListRow["Index"] = fileStartPack:GetAsNumber()

			StarterPack_ItemListRow["Level"] = fileStartPack:GetAsNumber()

			StarterPack_ItemListRow["Count"] = fileStartPack:GetAsNumber()

			StarterPack_ItemListRow["Option1"] = fileStartPack:GetAsNumber()

			StarterPack_ItemListRow["Option2"] = fileStartPack:GetAsNumber()

			StarterPack_ItemListRow["Option3"] = fileStartPack:GetAsNumber()

			StarterPack_ItemListRow["NewOption"] = fileStartPack:GetAsNumber()

			table.insert(StarterPack_ItemList,StarterPack_ItemListRow)

		end

		fileStartPack:Close()

	end
end


function StarterPack_GiveStarterPackEvent(aIndex)

    local UserClass = GetObjectClass(aIndex)
	LoadFileTxtEvent(aIndex);

	for n=1,#StarterPack_ItemListEvent,1 do
		if StarterPack_ItemListEvent[n].Class == -1 or StarterPack_ItemListEvent[n].Class == UserClass then

			if StarterPack_ItemListEvent[n].Group == 0 then

				ItemGiveEx(aIndex,StarterPack_ItemListEvent[n].Index,StarterPack_ItemListEvent[n].Level,StarterPack_ItemListEvent[n].Count,StarterPack_ItemListEvent[n].Option1,StarterPack_ItemListEvent[n].Option2,StarterPack_ItemListEvent[n].Option3,StarterPack_ItemListEvent[n].NewOption)

			elseif StarterPack_ItemListEvent[n].Group == 1 then

				EffectAdd(aIndex,1,StarterPack_ItemListEvent[n].Index,StarterPack_ItemListEvent[n].Count,0,0,0,0)

			end

		end

	end
	NoticeSend(aIndex,1,"Are you received your kit name: "..NamePackEvent);
end

function LoadFileTxtEvent(aIndex) 
	StarterPack_ItemListEvent = {}

	if(GetObjectLevel(aIndex) == 1 and GetObjectMasterLevel(aIndex) == 0) then
		fileStartPack = Reader:Load("..\\Data\\Script\\Data\\StarterPackEvent.txt")
		NamePackEvent = 'Open 25/06.';  
	end

	if fileStartPack ~= nil then

		while fileStartPack:GetLine("end") == true do

			local StarterPack_ItemListEventRow = {}

			StarterPack_ItemListEventRow["Class"] = fileStartPack:GetAsNumber()

			StarterPack_ItemListEventRow["Group"] = fileStartPack:GetAsNumber()

			StarterPack_ItemListEventRow["Index"] = fileStartPack:GetAsNumber()

			StarterPack_ItemListEventRow["Level"] = fileStartPack:GetAsNumber()

			StarterPack_ItemListEventRow["Count"] = fileStartPack:GetAsNumber()

			StarterPack_ItemListEventRow["Option1"] = fileStartPack:GetAsNumber()

			StarterPack_ItemListEventRow["Option2"] = fileStartPack:GetAsNumber()

			StarterPack_ItemListEventRow["Option3"] = fileStartPack:GetAsNumber()

			StarterPack_ItemListEventRow["NewOption"] = fileStartPack:GetAsNumber()

			table.insert(StarterPack_ItemListEvent,StarterPack_ItemListEventRow)

		end

		fileStartPack:Close()

	end
end