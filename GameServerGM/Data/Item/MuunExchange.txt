//Index   NeedItemIndex   NeedItemLevel   NeedItemCount   SwapItemIndex   SwapItemGrade   SwapItemCount
0         8432            0               2               8403            429             1
1         8432            0               2               8403            431             1
2         8432            0               3               8403            427             1
3         8432            0               2               8403            326             1
4         8432            0               2               8403            328             1
5         8432            0               2               8403            330             1
6         8432            0               2               8403            2               1       //Geppetto
7         8432            0               2               8403            4               1       //BlueFairy
8         8432            0               2               8403            8               1       //BabyBear
9         8432            0               2               8403            16              1       //TreasureHunter
10        8432            0               2               8403            18              1       //TreeElf
11        8432            0               2               8403            20              1       //GrassFairy
12        8432            0               2               8403            24              1       //SambaGirl
13        8432            0               2               8403            26              1       //Rio
14        8432            0               2               8403            28              1       //Bateria
15        8432            0               2               8403            32              1       //Tiger
16        8432            0               2               8403            34              1       //Libra
17        8432            0               2               8403            36              1       //Scorpio
18        8432            0               2               8403            38              1       //Rabbit
19        8432            0               2               8403            41              1       //Chicken
20        8432            0               2               8403            49              1       //KnightInstructor
21        8432            0               2               8403            55              1       //Adventurecat
22        8432            0               2               8403            57              1       //Dike
23        8432            0               2               8403            61              1       //Scarecrow
24        8432            0               2               8403            67              1       //SnowFairy
25        8432            0               2               8403            83              1       //Pilot
26        8432            0               2               8403            85              1       //SuperPanda
27        8432            0               2               8403            87              1       //BellyDancer
28        8432            0               2               8403            97              1       //BeachGirl
29        8432            0               2               8403            105             1       //FlyingBread
30        8432            0               2               8403            114             1       //Serufan
31        8432            0               2               8403            116             1       //Medusa
32        8432            0               2               8403            118             1       //Balrog
33        8432            0               2               8403            120             1       //Hellmine
34        8432            0               2               8403            124             1       //Hydra
35        8432            0               2               8403            136             1       //Desmodus

36        8432            0               2               8403            211             1       //EvolutionStone
37        8432            0               2               8403            229             1       //Wooparoo
38        8432            0               2               8403            235             1       //Skull
39        8432            0               2               8403            239             1       //MuunEnergyConvert
40        8432            0               2               8403            240             1       //WizardryStone
41        8432            0               2               8403            243             1       //Lycan
42        8432            0               2               8403            252             1       //Seiren
43        8432            0               2               8403            254             1       //Behemoth
44        8432            0               2               8403            258             1       //Mino
45        8432            0               2               8403            260             1       //Hound
46        8432            0               2               8403            266             1       //Cyclop
47        8432            0               2               8403            272             1       //Hawk
48        8432            0               2               8403            278             1       //Mantis
49        8432            0               2               8403            282             1       //Chafer
50        8432            0               2               8403            294             1       //SportyGirl
51        8432            0               2               8403            302             1       //Frozen
52        8432            0               2               8403            304             1       //Lure
53        8432            0               2               8403            308             1       //LuckyRabbit
54        8432            0               2               8403            322             1       //Kenturion
55        8432            0               2               8403            324             1       //Rocka
56        8432            0               2               8403            332             1       //PolarBear
57        8432            0               2               8403            334             1       //Present
58        8432            0               2               8403            336             1       //RockingHorse
59        8432            0               2               8403            340             1       //Lester
60        8432            0               2               8403            342             1       //Merino
61        8432            0               2               8403            344             1       //Splinter
62        8432            0               2               8403            346             1       //Mutation
63        8432            0               2               8403            348             1       //Fighter
64        8432            0               2               8403            409             1       //Heywood
65        8432            0               2               8403            411             1       //Branch
66        8432            0               2               8403            413             1       //Hook
67        8432            0               2               8403            417             1       //Mandora
68        8432            0               2               8403            419             1       //Treat
69        8432            0               2               8403            433             1       //SurfingGirl
70        8432            0               2               8403            439             1       //Commander
71        8432            0               2               8403            441             1       //LanceSoldier
72        8432            0               2               8403            443             1       //BowSoldier
73        8432            0               2               8403            445             1       //Hammer
74        8432            0               2               8403            451             1       //PumpkinGirl
75        8432            0               2               8403            453             1       //Ghost
76        8432            0               2               8403            455             1       //MummyBoy
77        8432            0               2               8403            457             1       //Chrysanthemum
78        8432            0               2               8403            459             1       //Maple
79        8432            0               2               8403            461             1       //Ginkgo
80        8432            0               2               8403            480             1       //ToySoldier
81        8432            0               2               8403            484             1       //Snowman
82        8432            0               2               8403            486             1       //BuskerMonkey
83        8432            0               2               8403            488             1       //DJMonkey
84        8432            0               2               8403            490             1       //SpaceMonkey
85        8432            0               2               8403            492             1       //LittleCupid
86        8432            0               2               8403            496             1       //HeartGirl
87        8432            0               2               8403            498             1       //CandyGirl
88        8432            0               2               8403            500             1       //LollipopMama
89        8432            0               2               8403            502             1       //SweetMuscleMan
//S16 pets    
90        8432            0               2               8403            126             1       //Lord Silvester  
91        8432            0               2               8403            140             1       //Fighter                    
92        8432            0               2               8403            142             1       //Flower Leader Leah  
93        8432            0               2               8403            144             1       //Shadow Phantom        
94        8432            0               2               8403            146             1       //Warehouse Guardian  
95        8432            0               2               8403            148             1       //Fairy Lara                
96        8432            0               2               8403            150             1       //Tablet's Elpis     

end