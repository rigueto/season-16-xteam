3
//Index   DropRate
0         10000
1	      10000
2	      10000
end

4
//Index   Section   SectionRate   MoneyAmount   OptionValue   DW   DK   FE   MG   DL   SU   RF   GL   RW   SL   GC
0         5         10000         100           32            1    1    1    1    1    1    1    1    1    1    1  
1         6         5000          100           32            1    1    1    1    1    1    1	 1    1    1    1
2         7         5000          100           32            1    1    1    1    1    1    1    1    1    1    1 
end

5
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration Comment 
14,486    0       0       *         *         *         *         *         *         *         0         //Ruud Box (1500)
end

6
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration Comment
0020      0       1       *         12        13        20        602       *         *         0               //Knight Blade Sword
0024	  0       1       *         12        13        20        602       *         *         0               //Daybreak
0021	  0       1       *         12        13        20        602       *         *         0               //Dark Reign Blade Sword
0025	  0       1       *         12        13        20        602       *         *         0               //Sword Dancer
1036      0       1       *         12        13        20        602       *         *         0               //Great Lord Scepter
1039      0       1       *         12        13        20        602       *         *         0               //Shining Scepter
1546      0       1       *         12        13        20        602       *         *         0               //Dragon Spear
2067      0       1       *         12        13        20        602       *         *         0               //Great Reign Crossbow
2068      0       1       *         12        13        20        602       *         *         0               //Arrow Viper CrossBow
2070      0       1       *         12        13        20        602       *         *         0               //Albatross Bow
2571      0       1       *         12        13        20        602       *         *         0               //Staff of Kundun
2573      0       1       *         12        13        20        602       *         *         0               //Platina Staff
2578      0       1       *         12        13        20        602       *         *         0               //Demonic Stick
2579      0       1       *         12        13        20        602       *         *         0               //Storm Blitz Stick
1551      0       1       *         12        13        20        602       *         *         0               //Pluma lancer
1552      0       1       *         12        13        20        602       *         *         0               //Vis lancer
0033	  0       1       *         12        13        20        602       *         *         0         		//Holy Storm Claw
0034      0       1       *         12        13        20        602       *         *         0               //Piercing Blade Glove
1053      0       1       *         12        13        20        602       *         *         0               //El hazard mace
1052      0       1       *         12        13        20        602       *         *         0               //Elemental Rune Mace
0067      0       1       *         12        13        20        602       *         *         0               //Short Sword Dacia
0068      0       1       *         12        13        20        602       *         *         0               //Short Sword Cookery
4,42      0       1       *         12        13        20        602       *         *         0               //Entropy Gun   
4,43      0       1       *         12        13        20        602       *         *         0               //Frere Magic Gun
end

7
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration Comment
6664      0       1       *         12        13        20        602       *         *         0               //Ring of Ice  
6665      0       1       *         12        13        20        602       *         *         0               //Ring of Poison
6677      0       1       *         12        13        20        602       *         *         0               //Ring of Fire
6678      0       1       *         12        13        20        602       *         *         0               //Ring of Earth
6679      0       1       *         12        13        20        602       *         *         0               //Ring of Wind
6680      0       1       *         12        13        20        602       *         *         0               //Ring of Magic
6668      0       1       *         12        13        20        602       *         *         0               //Pendant of Lighting
6669      0       1       *         12        13        20        602       *         *         0               //Pendant of Fire
6681      0       1       *         12        13        20        602       *         *         0               //Pendant of Ice
6682      0       1       *         12        13        20        602       *         *         0               //Pendant of Wind
6683      0       1       *         12        13        20        602       *         *         0               //Pendant of Water
6684      0       1       *         12        13        20        602       *         *         0               //Pendant of Ability
end