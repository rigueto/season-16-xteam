3
//Index   DropRate
0         10000
1         10000
2         10000
3         10000
4         10000
5         10000
6         10000
7         10000
8         10000
9         10000
end

4
//Index   Section   SectionRate   MoneyAmount   OptionValue   DW   DK   FE   MG   DL   SU   RF   GL   RW   SL   GC
0         5         10000          1000          0             1    1    1    1    1    1    1    1    1    1   1
1         5         10000          1000          0             1    1    1    1    1    1    1    1    1    1   1
2         5         10000          1000          0             1    1    1    1    1    1    1    1    1    1   1
3         5         10000          1000          0             1    1    1    1    1    1    1    1    1    1   1
4         5         10000          1000          0             1    1    1    1    1    1    1    1    1    1   1
5         5         10000          1000          0             1    1    1    1    1    1    1    1    1    1   1
6         5         10000          1000          0             1    1    1    1    1    1    1    1    1    1   1
7         5         10000          1000          0             1    1    1    1    1    1    1    1    1    1   1
8         5         10000          1000          0             1    1    1    1    1    1    1    1    1    1   1
9         5         10000          1000          0             1    1    1    1    1    1    1    1    1    1   1
end

5
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment               
14,003    0       30      *         *         *         *         *         *         *         *             //Large Healing Potion 
14,003    0       40      *         *         *         *         *         *         *         *             //Large Healing Potion 
14,003    0       50      *         *         *         *         *         *         *         *             //Large Healing Potion
7174      0       80      *         *         *         *         *         *         *         *             //Large Mana Potion
7174      0       80      *         *         *         *         *         *         *         *             //Large Mana Potion
7174      0       80      *         *         *         *         *         *         *         *             //Large Mana Potion

//
12,003    0       0       49        *         12        21        21        *         *         900          //Wings of Spirit 
12,004    0       0       49        *         12        21        21        *         *         900          //Wings of Soul     
12,005    0       0       49        *         12        21        21        *         *         900          //Wings of Dragon
12,006    0       0       49        *         12        21        21        *         *         900          //Wings of Darkness
12,042    0       0       49        *         12        21        21        *         *         900          //Wings of Despair
12,269    0       0       49        *         12        21        21        *         *         900          //Cloak of Limit
12,049    0       0       49        *         12        21        21        *         *         900          //Cape of Fighter 
13,030    0       0       49        *         12        21        21        *         *         900          //Cape of Lord 
//
00,015    0       0       44        12        12        21        12        *         *         900          //Giant Sword        
00,016    0       0       44        12        12        21        12        *         *         900          //Sword of Destruction
00,017    0       0       44        12        12        21        12        *         *         900          //Dark Breaker       
00,018    0       0       44        12        12        21        12        *         *         900          //Thunder Blade      
00,031    0       0       44        12        12        21        12        *         *         900          //Rune Blade
00,034    0       0       44        12        12        21        12        *         *         900          //Piercing Blade Glove
00,068    0       0       44        12        12        21        12        *         *         900          //Cookery Short Sword
01,008    0       0       44        12        12        21        12        *         *         900          //Crescent Axe
02,010    0       0       44        12        12        21        12        *         *         900          //Great Scepter
02,011    0       0       44        12        12        21        12        *         *         900          //Lord Scepter 
03,008    0       0       44        12        12        21        12        *         *         900          //Great Scythe 
03,009    0       0       44        12        12        21        12        *         *         900          //Bill of Balrog
03,010    0       0       44        12        12        21        12        *         *         900          //Dragon Spear
03,015    0       0       44        12        12        21        12        *         *         900          //Pluma Lance 
03,016    0       0       44        12        12        21        12        *         *         900          //Vis Lance 
04,010    0       0       44        12        12        21        12        *         *         900          //Arquebus       
04,013    0       0       44        12        12        21        12        *         *         900          //Light Crossbow 
04,014    0       0       44        12        12        21        12        *         *         900          //Serpent Crossbow
04,016    0       0       44        12        12        21        12        *         *         900          //Saint Crossbow
04,017    0       0       44        12        12        21        12        *         *         900          //Celestial Bow
05,008    0       0       44        *         12        21        12        *         *         900          //Staff of Destruction
05,009    0       0       44        *         12        21        12        *         *         900          //Dragon Soul Staff
05,017    0       0       44        *         12        21        12        *         *         900          //Ancient Stick
06,012    0       0       44        12        12        21        12        *         *         900          //Bronze Shield  
06,013    0       0       44        12        12        21        12        *         *         900          //Dragon Shield  
06,014    0       0       44        12        12        21        12        *         *         900          //Legendary Shield 
06,015    0       0       44        12        12        21        12        *         *         900          //Grand Soul Shield 
06,021    0       0       44        12        12        21        12        *         *         900          //Cross Shield 
06,030    0       0       44        12        12        21        12        *         *         900          //Pluma Shield
04,42     0       0       44        12        12        21        12        *         *         900          //Entropy Gun
04,43     0       0       44        12        12        21        12        *         *         900          //Frere Gun

//
07,041    0       0       44        *         12        21        12        *         *         900          //Ancient Mask  
08,041    0       0       44        *         12        21        12        *         *         900          //Ancient Armor  
09,041    0       0       44        *         12        21        12        *         *         900          //Ancient Pants  
10,041    0       0       44        *         12        21        12        *         *         900          //Ancient Gloves  
11,041    0       0       44        *         12        21        12        *         *         900          //Ancient Boots  
07,038    0       0       44        *         12        21        12        *         *         900          //Glorious Helm  
08,038    0       0       44        *         12        21        12        *         *         900          //Glorious Armor  
09,038    0       0       44        *         12        21        12        *         *         900          //Glorious Pants  
10,038    0       0       44        *         12        21        12        *         *         900          //Glorious Gloves  
11,038    0       0       44        *         12        21        12        *         *         900          //Glorious Boots   
08,037    0       0       44        *         12        21        12        *         *         900          //Valiant Armor  
09,037    0       0       44        *         12        21        12        *         *         900          //Valiant Pants  
10,037    0       0       44        *         12        21        12        *         *         900          //Valiant Gloves  
11,037    0       0       44        *         12        21        12        *         *         900          //Valiant Boots   
07,036    0       0       44        *         12        21        12        *         *         900          //Iris Helm  
08,036    0       0       44        *         12        21        12        *         *         900          //Iris Armor  
09,036    0       0       44        *         12        21        12        *         *         900          //Iris Pants  
10,036    0       0       44        *         12        21        12        *         *         900          //Iris Gloves  
11,036    0       0       44        *         12        21        12        *         *         900          //Iris Boots 
07,035    0       0       44        *         12        21        12        *         *         900          //Eclipse Helm  
08,035    0       0       44        *         12        21        12        *         *         900          //Eclipse Armor  
09,035    0       0       44        *         12        21        12        *         *         900          //Eclipse Pants  
10,035    0       0       44        *         12        21        12        *         *         900          //Eclipse Gloves  
11,035    0       0       44        *         12        21        12        *         *         900          //Eclipse Boots       
07,034    0       0       44        *         12        21        12        *         *         900          //Ashcrow Helm  
08,034    0       0       44        *         12        21        12        *         *         900          //Ashcrow Armor  
09,034    0       0       44        *         12        21        12        *         *         900          //Ashcrow Pants  
10,034    0       0       44        *         12        21        12        *         *         900          //Ashcrow Gloves  
11,034    0       0       44        *         12        21        12        *         *         900          //Ashcrow Boots 
07,094    0       0       44        *         12        21        12        *         *         900          //Hirat Helm  
08,094    0       0       44        *         12        21        12        *         *         900          //Hirat Armor  
09,094    0       0       44        *         12        21        12        *         *         900          //Hirat Pants  
10,094    0       0       44        *         12        21        12        *         *         900          //Hirat Gloves  
11,094    0       0       44        *         12        21        12        *         *         900          //Hirat Boots
07,119    0       0       44        *         12        21        12        *         *         900          //Kenaz Helm  
08,119    0       0       44        *         12        21        12        *         *         900          //Kenaz Armor  
09,119    0       0       44        *         12        21        12        *         *         900          //Kenaz Pants  
10,119    0       0       44        *         12        21        12        *         *         900          //Kenaz Gloves  
11,119    0       0       44        *         12        21        12        *         *         900          //Kenaz Boots  
07,176    0       0       44        *         12        21        12        *         *         900          //Slayer Demonic Helm  
08,176    0       0       44        *         12        21        12        *         *         900          //Slayer Demonic Armor  
09,176    0       0       44        *         12        21        12        *         *         900          //Slayer Demonic Pants  
10,176    0       0       44        *         12        21        12        *         *         900          //Slayer Demonic Gloves  
11,176    0       0       44        *         12        21        12        *         *         900          //Slayer Demonic Boots
07,059    0       0       44        *         12        21        12        *         *         900          //Sacred Fire Helm  
08,059    0       0       44        *         12        21        12        *         *         900          //Sacred Fire Armor  
09,059    0       0       44        *         12        21        12        *         *         900          //Sacred Fire Pants   
11,059    0       0       44        *         13        21        12        *         *         900          //Sacred Fire Boots 
7,209     0       0       44        *         13        21        12        *         *         900          //Frere Armor
8,209     0       0       44        *         13        21        12        *         *         900          //
9,209     0       0       44        *         13        21        12        *         *         900          //
10,209    0       0       44        *         13        21        12        *         *         900          //
11,209    0       0       44        *         13        21        12        *         *         900          //
13,003    0       0       *         *         *         *         *         *         *         900          //Horn of Dinorant
end