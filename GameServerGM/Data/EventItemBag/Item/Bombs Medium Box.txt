3
//Index   DropRate
0         10000
1         10000
end

4
//Index   Section   SectionRate   MoneyAmount   OptionValue   DW   DK   FE   MG   DL   SU   RF   GL   RW   SL   GC
0         5         10000         0	            0             1    0    0    0    0    0    0    0    0    0    0  
0         6         10000         0	            0             0    1    0    0    0    0    0    0    0    0    0 
0         7         10000         0	            0             0    0    1    0    0    0    0    0    0    0    0 
0         8         10000         0	            0             0    0    0    1    0    0    0    0    0    0    0 
0         9         10000         0	            0             0    0    0    0    1    0    0    0    0    0    0 
0         10        10000         0	            0             0    0    0    0    0    1    0    0    0    0    0 
0         11        10000         0	            0             0    0    0    0    0    0    1    0    0    0    0 
0         12        10000         0	            0             0    0    0    0    0    0    0    1    0    0    0 
0         13        10000         0	            0             0    0    0    0    0    0    0    0    1    0    0 
0         14        10000         0	            0             0    0    0    0    0    0    0    0    0    1    0 
0         15        10000         0	            0             0    0    0    0    0    0    0    0    0    0    1  
1         5         10000         0	            0             1    0    0    0    0    0    0    0    0    0    0  
1         6         10000         0	            0             0    1    0    0    0    0    0    0    0    0    0 
1         7         10000         0	            0             0    0    1    0    0    0    0    0    0    0    0 
1         8         10000         0	            0             0    0    0    1    0    0    0    0    0    0    0 
1         9         10000         0	            0             0    0    0    0    1    0    0    0    0    0    0 
1         10        10000         0	            0             0    0    0    0    0    1    0    0    0    0    0 
1         11        10000         0	            0             0    0    0    0    0    0    1    0    0    0    0 
1         12        10000         0	            0             0    0    0    0    0    0    0    1    0    0    0 
1         13        10000         0	            0             0    0    0    0    0    0    0    0    1    0    0 
1         14        10000         0	            0             0    0    0    0    0    0    0    0    0    1    0 
1         15        10000         0	            0             0    0    0    0    0    0    0    0    0    0    1  
end

5
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
7,18      0       1       *         *         13        20        602       *         *         0          //Helm Grand Soul
8,18      0       1       *         *         13        20        602       *         *         0          //Armo Grand Soul
9,18      0       1       *         *         13        20        602       *         *         0          //Pants Grand Soul
10,18     0       1       *         *         13        20        602       *         *         0          //Gloves Grand Soul
11,18     0       1       *         *         13        20        602       *         *         0          //Boots Grand Soul 
end

6
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
7,17      0       1       *         *         13        20        602       *         *         0          //Helm Dark Phoenix
8,17      0       1       *         *         13        20        602       *         *         0          //Armo Dark Phoenix
9,17      0       1       *         *         13        20        602       *         *         0          //Pants Dark Phoenix
10,17     0       1       *         *         13        20        602       *         *         0          //Gloves Dark Phoenix
11,17     0       1       *         *         13        20        602       *         *         0          //Boots Dark Phoenix 
end

7
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
7,19      0       1       *         *         13        20        602       *         *         0          	//Helm Holy Spirit
8,19      0       1       *         *         13        20        602       *         *         0          	//Armo Holy Spirit
9,19      0       1       *         *         13        20        602       *         *         0          	//Pants Holy Spirit
10,19     0       1       *         *         13        20        602       *         *         0          	//Gloves Holy Spirit
11,19     0       1       *         *         13        20        602       *         *         0          	//Boots Holy Spirit 
end

8
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
8,20      0       1       *         *         13        20        602       *         *         0          	//Armo Thunder Hawk
9,20      0       1       *         *         13        20        602       *         *         0          	//Pants Thunder Hawk
10,20     0       1       *         *         13        20        602       *         *         0          	//Gloves Thunder Hawk
11,20     0       1       *         *         13        20        602       *         *         0          	//Boots Thunder Hawk 
end

9
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
7,27      0       1       *         *         13        20        602       *         *         0          	//Helm Dark Steel
8,27      0       1       *         *         13        20        602       *         *         0          	//Armo Dark Steel
9,27      0       1       *         *         13        20        602       *         *         0          	//Pants Dark Steel
10,27     0       1       *         *         13        20        602       *         *         0          	//Gloves Dark Steel
11,27     0       1       *         *         13        20        602       *         *         0          	//Boots Dark Steel 
end

10
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
7,42      0       1       *         *         13        20        602       *         *         0          	//Helm Demoniac SUM
8,42      0       1       *         *         13        20        602       *         *         0          	//Armo Demoniac SUM
9,42      0       1       *         *         13        20        602       *         *         0          	//Pants Demoniac SUM
10,42     0       1       *         *         13        20        602       *         *         0          	//Gloves Demoniac SUM
11,42     0       1       *         *         13        20        602       *         *         0          	//Boots Demoniac SUM 
end

11
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
7,59      0       1       *         *         13        20        602       *         *         0          	//Helm Sacred fire
8,59      0       1       *         *         13        20        602       *         *         0          	//Armo Sacred fire
9,59      0       1       *         *         13        20        602       *         *         0          	//Pants Sacred fire
11,59     0       1       *         *         13        20        602       *         *         0          	//Boots Sacred fire 
end

12
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
//Set Hirat GL
3678      0       1       *         *         13        20        602       *         *         0          	//
4190      0       1       *         *         13        20        602       *         *         0          	// 
4702      0       1       *         *         13        20        602       *         *         0          	// 
5214      0       1       *         *         13        20        602       *         *         0          	// 
5726      0       1       *         *         13        20        602       *         *         0          	//  
end

13
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
//Set kanaz RM
3703      0       1       *         *         13        20        602       *         *         0         	//
4215      0       1       *         *         13        20        602       *         *         0         	//
4727      0       1       *         *         13        20        602       *         *         0         	//
5239      0       1       *         *         13        20        602       *         *         0         	//
5751      0       1       *         *         13        20        602       *         *         0         	// 
end

14
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
//Set Nightwing SL
3761      0       1       *         *         13        20        602       *         *         0          	//
4273      0       1       *         *         13        20        602       *         *         0          	//
4785      0       1       *         *         13        20        602       *         *         0          	//
5297      0       1       *         *         13        20        602       *         *         0          	//
5809      0       1       *         *         13        20        602       *         *         0          	// 
end

15
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
//Gun Frere
7,209     0       1       *         *         13        20        602       *         *         0          	//
8,209     0       1       *         *         13        20        602       *         *         0          	//
9,209     0       1       *         *         13        20        602       *         *         0          	//
10,20     0       1       *         *         13        20        602       *         *         0          	//
11,20     0       1       *         *         13        20        602       *         *         0          	// 
end
 