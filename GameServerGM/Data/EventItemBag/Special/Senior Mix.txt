
3
//Index   DropRate
0         10000
end

4
//Index   Section   SectionRate   MoneyAmount   OptionValue   DW   DK   FE   MG   DL   SU   RF   GL   RW   SL   GC
0         5         5000          0             0             1    1    1    1    1    1    1    1    1    1    1 
0         6         800           0             0             1    1    1    1    1    1    1    1    1    1    1 
0         7         200           0             0             1    1    1    1    1    1    1    1    1    1    1 
0         8         2000          0             0             1    1    1    1    1    1    1    1    1    1    1 
0         9         1000          0             4             1    1    1    1    1    1    1    1    1    1    1
0         10        1000          0             0             1    1    1    1    1    1    1    1    1    1    1 
end

//5  Golden Fenrir 50%
//6  Condor Feather 8%
//7  Garuda's Feather 2%
//8  Seed Sphere Lv. 6 20%
//9  Pack com 5 Bless of Light (Mid) 10%
//1  Rare item ticket lv 1  10%

5
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
6693      0       4       -1        -1        -1        -1        -1        -1        -1        0         //Horn of Fenrir Illusion Golden
end

6
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
6709      0       0       -1        -1        -1        -1        -1        -1         -1         0       //Condor Feather
end

7
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
7123      0       0       -1        -1        -1        -1        -1        -1         -1         0       //Garuda's Feather
end

8
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
6455      0       0       600       *         *         *         *         *         *         0          //Seed Sphere (Fire)     
6456      0       0       610       *         *         *         *         *         *         0          //Seed Sphere (Water)    
6457      0       0       610       *         *         *         *         *         *         0          //Seed Sphere (Ice)      
6458      0       0       600       *         *         *         *         *         *         0          //Seed Sphere (Wind)     
6459      0       0       620       *         *         *         *         *         *         0          //Seed Sphere (Lightning)
end

9
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
7432      0       0       -1        -1        -1        -1        -1        -1         -1         0       //Bless of Light (Mid)
7432      0       0       -1        -1        -1        -1        -1        -1         -1         0       //Bless of Light (Mid)
7432      0       0       -1        -1        -1        -1        -1        -1         -1         0       //Bless of Light (Mid)
7432      0       0       -1        -1        -1        -1        -1        -1         -1         0       //Bless of Light (Mid)
7432      0       0       -1        -1        -1        -1        -1        -1         -1         0       //Bless of Light (Mid)
end

10
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
14,58     0       0       *         *         *         *         *         *         0         0          //Rare Ticket Lvl1
end


//7
////Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
//6693      0       0       -1        -1        -1        -1        -1        -1        -1        0         //Horn of Fenrir Traditional Red
//end
//
//8
////Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
//6693      0       2       -1        -1        -1        -1        -1        -1        -1        0         //Horn of Fenrir Protect Blue
//end
//
//9
////Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
//6693      0       1       -1        -1        -1        -1        -1        -1        -1        0         //Horn of Fenrir Destroyer Black
//end
//
//10
////Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
//7621      0       0       -1        -1        -1        -1        -1        -1        -1        0         //Switch Scroll 10 dias
//end