//Class Group      Index   Level   Count     Option1   Option2   Option3   NewOption  //Comment
//Dark Wizard
0         0        6791    0       0         1         1         1         28         //1st Lucky Armor Ticket
0         0        6792    0       0         1         1         1         28         //1st Lucky Pants Ticket
0         0        6793    0       0         1         1         1         28         //1st Lucky Helm Ticket 
0         0        6794    0       0         1         1         1         28         //1st Lucky Gloves Ticket
0         0        6795    0       0         1         1         1         28         //1st Lucky Boots Ticket
0         0        2605    0       0         1         1         1         28         //[Bound] Legendary Staff
0         0        3109    0       0         1         1         1         28         //[Bound] Legendary Shield
0         0        6566    15      0         63        63        4         63         //Wing of Soul Lvl 2 Bound
//Class Group      Index   Level   Count     Option1   Option2   Option3   NewOption  //Comment
//Dark Knight
1       0          6791    0       0         1         1         1         28         //1st Lucky Armor Ticket
1       0          6792    0       0         1         1         1         28         //1st Lucky Pants Ticket
1       0          6793    0       0         1         1         1         28         //1st Lucky Helm Ticket
1       0          6794    0       0         1         1         1         28         //1st Lucky Gloves Ticket
1       0          6795    0       0         1         1         1         28         //1st Lucky Boots Ticket
1       0          0048    0       0         1         1         1         28         //[Bound] Sword of Destruction
1       0          6567    15      0         63        63        4         63         //Wing of Dragon Lvl 2 bound
//Class Group      Index   Level   Count     Option1   Option2   Option3   NewOption  //Comment
//Elfa
2       0          6791    0       0         1         1         1         28         //1st Lucky Armor Ticket
2       0          6792    0       0         1         1         1         28         //1st Lucky Pants Ticket
2       0          6793    0       0         1         1         1         28         //1st Lucky Helm Ticket 
2       0          6794    0       0         1         1         1         28         //1st Lucky Gloves Ticket
2       0          6795    0       0         1         1         1         28         //1st Lucky Boots Ticket
2       0          2077    0       0         1         1         1         28         //[Bound] Celestial Bow
2       0          6568    15      0         63        63        4         63         //Wings of Spirit - lvl 2 bound
//Class Group      Index   Level   Count     Option1   Option2   Option3   NewOption  //Comment
//MG 
3       0          6791    0       0         1         1         1         28         //1st Lucky Armor Ticket
3       0          6792    0       0         1         1         1         28         //1st Lucky Pants Ticket
//3       0          6793    0       0         1         1         1         28         //1st Lucky Helm Ticket 
3       0          6794    0       0         1         1         1         28         //1st Lucky Gloves Ticket
3       0          6795    0       0         1         1         1         28         //1st Lucky Boots Ticket
3       0          3109    0       0         1         1         1         28         //[Bound] Legendary Shield
3       0          2571    0       0         1         1         1         28         //Kundun Staff
3       0          6577    15      0         63        63        4         63         //Wings of Darkness Lvl 3 bound
//Class Group      Index   Level   Count     Option1   Option2   Option3   NewOption  //Comment
//Dark Lord 
4       0          6791    0       0         1         1         1         28          //1st Lucky Armor Ticket
4       0          6792    0       0         1         1         1         28          //1st Lucky Pants Ticket
4       0          6793    0       0         1         1         1         28          //1st Lucky Helm Ticket 
4       0          6794    0       0         1         1         1         28          //1st Lucky Gloves Ticket
4       0          6795    0       0         1         1         1         28          //1st Lucky Boots Ticket
4       0          1048    0       0         1         1         1         28          //[Bound] Lord's Scepter
4       0          3110    0       0         1         1         1         28          //[Bound] Cross Shield
4       0          6570    15      0         63        63        4         63          //Cape of Lord  Lvl 2 bound
//Class Group      Index   Level   Count     Option1   Option2   Option3   NewOption  //Comment
//Sumoner
5       0          6791    0       0         1         1         1         28         //1st Lucky Armor Ticket
5       0          6792    0       0         1         1         1         28         //1st Lucky Pants Ticket
5       0          6793    0       0         1         1         1         28         //1st Lucky Helm Ticket 
5       0          6794    0       0         1         1         1         28         //1st Lucky Gloves Ticket
5       0          6795    0       0         1         1         1         28         //1st Lucky Boots Ticket
5       0          2606    0       0         1         1         1         28         //[Bound] Red Winged Stick
5       0          2607    0       0         1         1         1         28         //[Bound] Book of Lagle
5       0          6571    15      0         63        63        4         63         //wing of dispair Lvl 2 bound

//Class Group      Index   Level   Count     Option1   Option2   Option3   NewOption  //Comment
//Rage Fighter
6       0          6791    0       0         1         1         1         28         //1st Lucky Armor Ticket
6       0          6792    0       0         1         1         1         28         //1st Lucky Pants Ticket
6       0          6793    0       0         1         1         1         28         //1st Lucky Helm Ticket 
//6       0          6794    0       0         1         1         1         28         //1st Lucky Gloves Ticket
6       0          6795    0       0         1         1         1         28         //1st Lucky Boots Ticket
6       0          0050    0       0         1         1         1         28         //[Bound] Holy Storm Claws
6       0          6572    15      0         63        63        4         63         //Cape of Fighter Lvl 2 bound
//Class Group      Index   Level   Count     Option1   Option2   Option3   NewOption  //Comment
//Grow Lancer
7       0          6791    0       0         1         1         1         28         //1st Lucky Armor Ticket
7       0          6792    0       0         1         1         1         28         //1st Lucky Pants Ticket
7       0          6793    0       0         1         1         1         28         //1st Lucky Helm Ticket 
7       0          6794    0       0         1         1         1         28         //1st Lucky Gloves Ticket
7       0          6795    0       0         1         1         1         28         //1st Lucky Boots Ticket
7       0          1557    0       0         1         1         1         28         //[Bound] Pluma Lance
7       0          3111    0       0         1         1         1         28         //[Bound] Pluma Shield
7       0          6573    15      0         63        63        4         63         //Clocks of limit Lvl 2 bound
//Class Group      Index   Level   Count     Option1   Option2   Option3   NewOption  //Comment
//Rune Mage
8       0          6791    0       0         1         1         1         28         //1st Lucky Armor Ticket
8       0          6792    0       0         1         1         1         28         //1st Lucky Pants Ticket
8       0          6793    0       0         1         1         1         28         //1st Lucky Helm Ticket 
8       0          6794    0       0         1         1         1         28         //1st Lucky Gloves Ticket
8       0          6795    0       0         1         1         1         28         //1st Lucky Boots Ticket
8       0          1061    0       0         1         1         1         28         //[Bound] Elemental Rune Mace
8       0          3115    0       0         1         1         1         28         //[Bound] Elemental Shield
8       0          6566    15      0         63        63        4         63         //Wing of Soul Lvl 2 bound
//Class Group      Index   Level   Count     Option1   Option2   Option3   NewOption  //Comment
//Slayer
9       0          6791    0       0         1         1         1         28         //1st Lucky Armor Ticket
9       0          6792    0       0         1         1         1         28         //1st Lucky Pants Ticket
9       0          6793    0       0         1         1         1         28         //1st Lucky Helm Ticket 
9       0          6794    0       0         1         1         1         28         //1st Lucky Gloves Ticket
9       0          6795    0       0         1         1         1         28         //1st Lucky Boots Ticket
9       0          0065    0       0         1         1         1         28         //[Bound] Cookery Short Sword
9       0          0065    0       0         1         1         1         28         //[Bound] Cookery Short Sword
9       0          6567    15      0         63        63        4         63         //Wing of Dragon Lvl 2 bound
//Class Group      Index   Level   Count     Option1   Option2   Option3   NewOption  //Comment
//Gun
10      0          6791    0       0         1         1         1         28         //1st Lucky Armor Ticket
10      0          6792    0       0         1         1         1         28         //1st Lucky Pants Ticket
10      0          6793    0       0         1         1         1         28         //1st Lucky Helm Ticket 
10      0          6794    0       0         1         1         1         28         //1st Lucky Gloves Ticket
10      0          6795    0       0         1         1         1         28         //1st Lucky Boots Ticket
10      0          2103    0       0         1         1         1         28         //[Bound] Frere Magic Gun
10      0          2103    0       0         1         1         1         28         //[Bound] Frere Magic Gun
10      0          6566    15      0         63        63        4         63         //Wing of Soul Lvl 2  bound
end


//Class = Classe do Char

//Group = 0 Item e 1 Effect

//Index = 512xCategoria+ID para Item e Index do Effect no Effect.txt

//Level = Level do Item, 0 para Effect

//Count = Para Item � a Durabilidade 0 = 255, para Effect o tempo em segundos

//Option1 = Skill 0 para N�O, e 1 para SIM, Itens que n�o tem skill precisa colocar 0, para Effect deixa 0

//Option2 = Luck 0 para N�O, e 1 para SIM, para Effect deixa 0

//Option3 = Life (1 +4, 2 +8, 3 +12, 4 +16...), 0 para Effect

//NewOption = Op��es Excelentes

//Op��es Excelente para Armors, Helms, Pants, Gloves, Boots, Shields and Rings
//1  - Increase Zen After Hunt 40%
//2  - Defense Succes Rate 10%
//4  - Reflect Damage 5%
//8  - Damage Decrease 4%
//16 - Increase MP 4%
//32 - Increase HP 4%

//Op��es Excelente para Weapons and Pendants
//1  - Increase mana after monster + mana/8
//2  - Increase life after monster + life/8
//4  - Increase Speed + 7
//8  - Increase Damage + 2%
//16 - Increase Damage + level/20
//32 - Excellent Damage Rate + 10%

//Para juntar as op��es Exemplo: Speed +7 + Exc 10% = Soma 4 + 32 = 36