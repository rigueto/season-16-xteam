3
//Index   DropRate
0         10000
end

4
//Index   Section   SectionRate   MoneyAmount   OptionValue   DW   DK   FE   MG   DL   SU   RF   GL   RW   SL   GC
0         5         5000          0             4             1    1    1    1    1    1    1    1    1    1    1 
0         6         5000          0             0             1    1    1    1    1    1    1    1    1    1    1 
end

5
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
12,015    0       0       *         *         *         *         *         *         *         0          //Jewel of Chaos
14,013    0       0       *         *         *         *         *         *         *         0          //Jewel of Bless
14,014    0       0       *         *         *         *         *         *         *         0          //Jewel of Soul
14,016    0       0       *         *         *         *         *         *         *         0          //Jewel of Life
14,022    0       0       *         *         *         *         *         *         *         0          //Jewel of Creation
end

6
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration Comment 
12,221      0       0       *         *         *         *         *        *          0         0         //Errtel of Anger   
12,231      0       0       *         *         *         *         *        *          0         0         //Errtel of Blessing
12,241      0       0       *         *         *         *         *        *          0         0         //Errtel of Integrity
12,251      0       0       *         *         *         *         *        *          0         0         //Errtel of Divinity
end