3
//Index   DropRate
0         10000
1         10000
//2         10000
end

4
//Index   Section   SectionRate   MoneyAmount   OptionValue   DW   DK   FE   MG   DL   SU   RF   GL   RW   SL   GC
0         5         10000         100           0             1    1    1    1    1    1    1    1    1    1    1
1         6         10000         100           0             1    1    1    1    1    1	1    1    1    1    1
//2         7         10000         100           0             1    1    1    1    1    1	1    1    1    1    1
end

5
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration     	Comment
6256      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Fire)"     
6257      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Water)"    
6258      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Ice)"      
6259      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Wind)"     
6260      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Lightning)"
6261      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Earth)""    
6262      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Fire)"     
6263      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Water)"    
6264      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Ice)"      
6265      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Wind)"     
6266      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Lightning)"
6267      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Earth)"    
6268      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Fire)"     
6269      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Water)"    
6270      0       0       610       *         *         *         *        *         0         0         //Seed Sphere (Ice)"      
6271      0       0       600       *         *         *         *        *         0         0         //Seed Sphere (Wind)"     
6272      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Lightning)"
6273      0       0       620       *         *         *         *        *         0         0         //Seed Sphere (Earth)
end

6
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration     	Comment
6676      0       0       *         *         *         *         *         *        0          86400      //Wizards Ring
end
 