3
//Index   DropRate
0         10000
1         10000
2         10000
3         10000
4         10000
end

4
//Index   Section   SectionRate   MoneyAmount   OptionValue   DW   DK   FE   MG   DL   SU   RF   GL   RW   SL   GC
0         5         10000         0             04            1    1    1    1    1    1    1    1    1    1    1 
1         6         10000         100           32            1    1    1    1    1    1    1    1    1    1    1
2         7         10000         100           04            1    1    1    1    1    1    1    1    1    1    1
3         8         4000          100           32            1    1    1    1    1    1    1    1    1    1    1
3         9         6000          100           04            1    1    1    1    1    1	1    1    1    1    1 
4         10        10000         100           32            1    1    1    1    1    1	1    1    1    1    1 
end

5
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration Comment 
14,506    0       0       *         *         *         *         *         *         *         0         //Ruud Box (5000)
14,506    0       0       *         *         *         *         *         *         *         0         //Ruud Box (5000)
14,506    0       0       *         *         *         *         *         *         *         0         //Ruud Box (5000)
end

6
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
6354        0       0       *         *         *         *         *        *        5         0      //Empire Guardians' Stronghold"          
6357        0       0       *         *         *         *         *        *        5         0      //Antonia's Sword"                 
6450        0       0       *         *         *         *         *        *        5         0      //Runedil's Goldentune Harp" 
6451        0       0       *         *         *         *         *        *        5         0      //Lemuria's Orb"             
6452        0       0       *         *         *         *         *        *        5         0      //Norrwen's Bloodstring Lyra"  
end

7
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration Comment
14,59      0       0       *         *         *         *         *        *         0         0         //Rare Ticket Lvl2
14,59      0       0       *         *         *         *         *        *         0         0         //Rare Ticket Lvl2
14,59      0       0       *         *         *         *         *        *         0         0         //Rare Ticket Lvl2
14,59      0       0       *         *         *         *         *        *         0         0         //Rare Ticket Lvl2
14,59      0       0       *         *         *         *         *        *         0         0         //Rare Ticket Lvl2
end

8
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
7122      0       0       -1        -1       -1        -1        -1        -1        -1         0  	       //Garuda's Flame           
end

9
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
6405      0       0       *         *         *         *         *        *          0         0         //Errtel of Radiance
6405      0       0       *         *         *         *         *        *          0         0         //Errtel of Radiance
end

10
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
7124      0       25      -1        -1        -1        -1        -1        -1        -1        0  	       //golden sentence
end