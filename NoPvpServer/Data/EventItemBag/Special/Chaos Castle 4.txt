 3
//Index   DropRate
0         10000
1         10000
end

4
//Index   Section   SectionRate   MoneyAmount   OptionValue   DW   DK   FE   MG   DL   SU   RF   GL   RW   SL   GC
0         5         10000         1             32            1    1    1    1    1    1    1	 1    1    1    1   
1         6         2000          1             32            1    1    1    1    1    1    1    1    1    1    1     
1         7         8000          1             4             1    1    1    1    1    1    1    1    1    1    1
end

5
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration Comment 
14,504    0       0       *         *         *         *         *         *         *         0         //Ruud Box (1200)
end

6
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment

0060      0       0       *         13        602       *         602       *         *         0        //Archangel Absolute Claws
1058      0       0       *         13        602       *         602       *         *         0        //Absolute Mace of Archangel
2066      0       0       *         13        602       *         602       *         *         0        //Divine Crossbow of Archangel
2570      0       0       *         13        602       *         602       *         *         0        //Divine Staff of Archangel
2596      0       0       *         13        602       *         602       *         *         0        //Divine Stick of the Archangel
0019      0       0       *         13        602       *         602       *         *         0        //Divine Sword of Archangel
0076      0       0       *         13        602       *         602       *         *         0        //Divine Short Sword of Arch
1037      0       0       *         13        602       *         602       *         *         0        //Divine Scepter of Archangel
2596      0       0       *         13        602       *         602       *         *         0        //Divine Stick of the Archangel
1058      0       0       *         13        602       *         602       *         *         0        //Divine Rune Mace of
2596      0       0       *         13        602       *         602       *         *         0        //DIVINE STICK OF ARC
4,46      0       0       *         13        602       *         602       *         *         0        //DIVINE Absolute Magic Gun
end

7
//Index   Level   Grade   Option0   Option1   Option2   Option3   Option4   Option5   Option6   Duration   Comment
7190      0       2       -1        -1        -1        -1        -1        -1        -1        0         //Jewel of Creation 
7184      0       2       -1        -1        -1        -1        -1        -1        -1        0         //Jewel of Life
end